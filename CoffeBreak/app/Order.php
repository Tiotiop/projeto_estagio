<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $order = [
        'user_id',
        'order_user',
        'coffee_id',
        'variety',
        'owner_id',
        'owner',
        'owner_email',
        'owner_phone',
        'order_quantity',
        'total_price',
        'user_payment_confirm',
        'owner_payment_confirm'
    ];
}
