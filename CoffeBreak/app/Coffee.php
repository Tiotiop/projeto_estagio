<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coffee extends Model
{
    protected $fillable = [
        'name',
        'unit_price',
        'quantity',
        'description',
        'owner_id',
        'owner_name'
    ];

    public function owner() {
        $this->belongsTo('App\User');
    }

}
