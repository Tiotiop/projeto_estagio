<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coffee;

use Illuminate\Support\Facades\DB;

class CoffeesController extends Controller
{
    public function create() {
        if (auth()->check()) {
            return view('/cadastroItem');
        }
        return view('/login');
    }

    public function store() {
        $validated_data = request()->validate([
            'name' => 'required|max:255',
            'unit_price' => 'required|starts_with:1,2,3,4,5,6,7,8,9',
            'quantity' => 'required|numeric|min:1',
        ]);
        $data = request()->all();
        $data['unit_price'] = str_replace(',','.', $data['unit_price']);

        $coffee = new Coffee ($data);

        $coffee->save();

        return redirect('/homeLoged');
    }

    public function coffeeInfo($id) {
        $coffee = DB::table('coffees')->where('id', $id)->get()->toArray();
        return view('coffee')->with('coffee', $coffee);
    }


}
