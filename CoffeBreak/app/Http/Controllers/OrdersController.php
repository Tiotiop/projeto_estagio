<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use Illuminate\Support\Arr;


class OrdersController extends Controller
{
    public function placeOrder() {
        if (auth()){
            $data_validation = request()->validate([
                'quantity' => 'required|min:1'
            ]);
                $order_data = request([
                    'coffee_id',
                    'variety',
                    'owner_id',
                    'owner_name',
                    'unit_price',
                    'quantity',
                ]);
                $coffee_quantity = DB::select('select quantity from coffees where id = ?', [$order_data['coffee_id']]);

            if ($order_data['quantity'] <= $coffee_quantity[0]->quantity) {
                $owner_data = DB::table('users')->where('id', $order_data['owner_id'])->get()->first();
                $total_price = $order_data['quantity'] * $order_data['unit_price'];

                $order = new Order;
                $order->user_id = auth()->user()->id;
                $order->order_user = auth()->user()->name;
                $order->coffee_id = $order_data['coffee_id'];
                $order->variety = $order_data['variety'];
                $order->owner_id = $order_data['owner_id'];
                $order->owner = $order_data['owner_name'];
                $order->owner_email = $owner_data->email;
                $order->owner_phone = $owner_data->telephone;
                $order->order_quantity = $order_data['quantity'];
                $order->total_price = $total_price;
                if(auth()->user()->id == $order_data['owner_id']) {
                    $order->user_payment_confirm = true;
                    $order->owner_payment_confirm = true;
                } else {
                    $order->user_payment_confirm = false;
                    $order->owner_payment_confirm = false;
                }

                $order->save();

                DB::update('update coffees set quantity = ? where id = ?', [$coffee_quantity[0]->quantity - $order_data['quantity'], $order_data['coffee_id']]);
                return redirect('homeLoged');
            } else {
                return back()->withErrors(['message1' => 'Não é possível pegar mais cafés que o disponível.']);
            }
        } else {
            return view('login');
        }
    }

    public function pay() {
        $request = request()->all();
        $orders_ids = array();
        for ($i = 1; $i < count($request); $i++) {
            $id = request()->get('order_id'.$i);
            $orders_ids = Arr::add($orders_ids, 'order_id'.$i, $id);
        }

        foreach($orders_ids as $order_id) {
            DB::table('orders')->where('order_id', $order_id)->update(['user_payment_confirm' => true]);
        }
        return redirect('pagarCafes')->with('message', 'Pagamento registrado! Aguarde confirmação do pagamento pelo dono');
    }

    public function confirmPayment() {
        $request = request()->all();
        $orders_ids = array();
        for ($i = 1; $i < count($request); $i++) {
            $id = request()->get('order_id'.$i);
            $orders_ids = Arr::add($orders_ids, 'order_id'.$i, $id);
        }

        foreach($orders_ids as $order_id) {
            DB::table('orders')->where('order_id', $order_id)->update(['owner_payment_confirm' => true]);
        }

        return redirect('confirmarPagamentos')->with('message', 'Pagamento confirmado!');
    }
}
