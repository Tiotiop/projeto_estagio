<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index(){
        $coffees = DB::table('coffees')->get()->toArray();
        return view('/home', compact('coffees'));
    }
}
