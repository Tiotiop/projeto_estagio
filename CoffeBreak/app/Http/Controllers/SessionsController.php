<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SessionsController extends Controller
{
    public function create() {
        return view('login');
    }

    public function store() {
        if (auth()->attempt(request(['email', 'password'])) == false) {
            return back()->withErrors([
                'message' => 'Email ou senha incorretos, por favor tente novamente.'
            ]);
        }
        return redirect()->to('/homeLoged');
    }

    public function destroy() {
        auth()->logout();

        return redirect()->to('/');
    }

    public function index() {
        $coffees = DB::table('coffees')->get()->toArray();
        return view('homeLoged', compact('coffees'));
    }
}
