<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{
    public function create() {
        return view('register');
    }

    public function store(){
        $validated_data = request()->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'CPF' => 'required|size:11',
            'telephone' => 'required|between:9,11'
        ]);

        $user = User::create(request(['name', 'email', 'CPF', 'telephone', 'password']));

        auth()->login($user);

        return redirect('/homeLoged');
    }
}
