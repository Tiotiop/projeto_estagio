<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class ProfilesController extends Controller
{
    public function create() {
        return view('perfil', [
            'id'=> auth()->user()->id,
            'name' => auth()->user()->name,
            'email' => auth()->user()->email,
            'CPF' => auth()->user()->CPF,
            'telefone' => auth()->user()->telephone
        ]);
    }

    public function getCoffees() {
        $myCoffees = DB::table('coffees')->get()->where('owner_id',auth()->user()->id)->toArray();
        return view('meusCafes', compact('myCoffees'));
    }

    public function getTakenCoffees() {
        $coffees = DB::table('orders')->get()->where('user_id',auth()->user()->id)->toArray();
        return view('relatorioPegos', compact('coffees'));
    }

    public function remove() {
        $data = request()->all();
        DB::table('coffees')->delete($data['id']);
        return redirect('/homeLoged');
    }

    public function getUnpaidCoffees() {
        $unpaid_coffees = DB::table('orders')->select()->where(['user_id' => auth()->user()->id, 'user_payment_confirm' => false])->get()->toArray();
        return view('pagarCafes', compact('unpaid_coffees', $unpaid_coffees));
    }

    public function getUnpaidCoffeesByUser() {
        $owed_owners_ids =  DB::table('orders')->select('owner_id')->where(['user_id' => auth()->user()->id, 'user_payment_confirm' => false])->get('owner_id')->toArray();
        $array_ids = array();
        $key = 0;
        foreach($owed_owners_ids as $element) {
            $array_ids = Arr::add($array_ids, 'id'.$element->owner_id, $element->owner_id);
            $key++;
        }
        $owed_users = array();
        foreach($array_ids as $id) {
            $owner_info = DB::table('users')->select()->where('id', $id)->get()->first();
            $owner_name = $owner_info->name;
            $unpaid_coffees = DB::table('orders')->select()->where(['user_id' => auth()->user()->id, 'owner_id'=> $id , 'user_payment_confirm' => false])->get()->toArray();

            $orders_ids = array();
            $idx = 0;
            $sum = 0;
            foreach ($unpaid_coffees as $coffee) {
                if ($coffee->owner_id == $id) {
                    $orders_ids = Arr::add($orders_ids, 'order_id'.$idx, $coffee->order_id);
                    $sum += $coffee->total_price;
                    $idx++;
                }
            }
            $owed_user = array('orders_ids' => $orders_ids, 'owner_id' => $id, 'owner_name' => $owner_name, 'sum' => $sum);
            $owed_users = Arr::add($owed_users, 'owed_user'.$id, $owed_user);
        }
        return view('pagarCafes')->with('owed_users', $owed_users);
    }

    public function getConfirmationRequests() {
        $all_ids = DB::table('orders')->select('user_id')->where(['owner_id' => auth()->user()->id, 'user_payment_confirm' => true, 'owner_payment_confirm' => false])->get()->toArray();
        $unique_ids = array();
        foreach($all_ids as $element) {
            $unique_ids = Arr::add($unique_ids, 'id'.$element->user_id, $element->user_id);
        }

        $confirmation_requests = array();
        foreach($unique_ids as $id) {
            $user_info = DB::table('users')->select()->where('id', $id)->get()->first();
            $user_name = $user_info->name;
            $user_unconfirmed_orders = DB::table('orders')->select()->where(['user_id' => $id, 'user_payment_confirm' => true, 'owner_payment_confirm' => false])->get()->toArray();

            $orders_ids = array();
            $idx = 0;
            $sum = 0;
            foreach($user_unconfirmed_orders as $order) {
                if ($order->user_id == $id) {
                    $orders_ids = Arr::add($orders_ids, 'id'.$idx, $order->order_id);
                    $sum += $order->total_price;
                    $idx++;
                }
            }
            $confirmation_request = array('orders_ids' => $orders_ids, 'user_id' => $id, 'user_name' => $user_name, 'sum' => $sum);
            $confirmation_requests = Arr::add($confirmation_requests, 'request'.$id, $confirmation_request);
        }

        return view('confirmarPagamentos')->with('confirmation_requests', $confirmation_requests);
    }
}
