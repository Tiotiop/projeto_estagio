<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Coffe-Break</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/business-casual.min.css" rel="stylesheet">

</head>

<body>

  <h1 class="site-heading text-center text-white d-none d-lg-block">
    <span class="site-heading-upper text-primary mb-3">Sistema para consignação de capsulas de café</span>
    <span class="site-heading-lower">Coffee Break</span>
  </h1>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
      <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Coffe-Break</a>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="http://127.0.0.1:8000">Home
            </a>
          </li>
          <li class="nav-item active px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="http://127.0.0.1:8000/sobre">Sobre</a>
          </li>
          <li class="nav-item px-lg-4">
            <a class="nav-link text-uppercase text-expanded" style="color: #e6a756" href="http://127.0.0.1:8000/register" onclick="window.location='{{url('register')}}'">Cadastre-se</a>
            {{-- <button onclick="window.location='{{url('register')}}'" class="btn btn-info mr-2 my-2 my-sm-0" type="button">Cadastrar</button> --}}
          </li>
          <li class="nav-item px-lg-4">
            <a class="nav-link text-uppercase text-expanded" style="color: #e6a756" href="http://127.0.0.1:8000/login" onclick="window.location='{{url('login')}}'" >Login</a>
            {{-- <button onclick="window.location='{{url('login')}}'" class="btn btn-success my-2 my-sm-0" type="button">Login</button> --}}
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section class="page-section about-heading">
    <div class="container">
      <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" style="width: 1100px; height: 360px;" src="img/about.jpg" alt="">
      <div class="about-heading-content">
        <div class="row">
          <div class="col-xl-9 col-lg-10 mx-auto">
            <div class="bg-faded rounded p-5">
              <h2 class="section-heading mb-4">
                <span class="section-heading-upper">Sistema para consignação de café</span>
                <span class="section-heading-lower">Sobre o nosso sistema</span>
              </h2>
              <p>A proposta do projeto era a automatizar o atual forma de consignações de café na sala dos professores
                  de ciência da computação e engenharia de software, melhorando a organizando de
                  estoque e de consumo das capsulas consumidas.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include ('components.footer')

  <!-- Bootstrap core JavaScript -->
  <script src="public/jquery/jquery.min.js"></script>
  <script src="public/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
