<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Coffe-Break</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <link href="css/business-casual.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-white" style="font-family: Raleway; background-image: linear-gradient(rgba(47,23,15,.65),rgba(47,23,15,.65)),url(../img/bg.jpg);">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #774E19 ">
        <a class="navbar-brand" href="http://127.0.0.1:8000/homeLoged">CoffeBreak</a>
        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> --}}

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" style="background-color: #CE862A"
                            type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Bem-Vindo {{auth()->user()->name}}
                        </button>
                        <div class="dropdown-menu " style="background-color: #CE862A; "
                            aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item text-white" style="background-color: transparent"
                                href="http://127.0.0.1:8000/perfil">Perfil</a>
                            <a class="dropdown-item text-white" style="background-color: transparent"
                                href="http://127.0.0.1:8000/cadastroItem">Cadastrar
                                Capsula</a>
                            <a class="dropdown-item text-white" style="background-color: transparent"
                                href="/logout">Sair</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container" style="margin-top: 100px">
        <div class="card " style="border-width: 6px; border-color: #774E19; background-color: rgb(238, 156, 50)">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <h2>{{$coffee[0]->name}}</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row col-md-12">
                            <h4 class="mb-0">Dono: {{$coffee[0]->owner_name}}</h4>
                        </div>
                        <div class="form-group row col-md-12">
                            <h4 class="mb-0">Preço: R$ {{number_format($coffee[0]->unit_price,2, ',', '.')}}</h4>
                        </div>
                        <div class="form-group row col-md-12">
                            <div class="mb-1 mt-2">Quantidade disponível: {{$coffee[0]->quantity}}</div>
                        </div>
                        <div class="form-group row col-md-12">
                            <p class="card-text mb-auto mt-3">{{$coffee[0]->description}} </p>
                        </div>
                        @if ($coffee[0]->quantity > 0)
                        <form action="/coffee/{{$coffee[0]->id}}" method="post">
                            @csrf
                            <input type="hidden" name="coffee_id" value="{{$coffee[0]->id}}">
                            <input type="hidden" name="variety" value="{{$coffee[0]->name}}">
                            <input type="hidden" name="owner_id" value="{{$coffee[0]->owner_id}}">
                            <input type="hidden" name="owner_name" value="{{$coffee[0]->owner_name}}">
                            <input type="hidden" name="unit_price" value="{{$coffee[0]->unit_price}}">
                            <input class="mt-3 form-control" style="width: 126px;
                            display: inline-block;
                            border-color: #584021; border-width: 4px" type="number" min="0" name="quantity">
                            <button class="btn btn-primary" style="margin-bottom: 5px; background-color: #4A300F;"
                                type="submit">Pegar</button>
                        </form>
                        @else
                        <p>ESGOTADO</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
