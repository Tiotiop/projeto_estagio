<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #E7A046 ">
    <a class="navbar-brand" style="font-weight:600;  color: #3a2a16" href="http://127.0.0.1:8000/homeLoged">CoffeBreak</a>
    {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button> --}}

    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" style=" color: #3a2a16; font-weight:500; background-color: #ffd48f"
                        type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Bem-Vindo {{auth()->user()->name}}
                    </button>
                    <div class="dropdown-menu " style=" background-color: #ffd48f; "
                        aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" style="  color: #3a2a16; font-weight:500; background-color: transparent"
                            href="http://127.0.0.1:8000/perfil">Perfil</a>
                        <a class="dropdown-item" style=" color: #3a2a16; font-weight:500; background-color: transparent"
                            href="http://127.0.0.1:8000/cadastroItem">Cadastrar
                            Capsula</a>
                        <a class="dropdown-item" style=" color: #3a2a16; font-weight:500; background-color: transparent"
                            href="/logout">Sair</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
