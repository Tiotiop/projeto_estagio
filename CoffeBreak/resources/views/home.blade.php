<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Coffe-Break</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">

    <style>

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">

</head>

<body style="font-family: Raleway">
    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="">
            <h1 class="site-heading text-center text-white d-none d-lg-block">
                <span class="site-heading-upper text-primary mb-3">Sistema para consignação de capsulas de café</span>
                <span class="site-heading-lower">Coffee Break</span>
            </h1>
            <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
                <div class="container">
                    <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Coffe Break</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item active px-lg-4">
                                <a class="nav-link text-uppercase text-expanded" href="http://127.0.0.1:8000">Home
                                </a>
                            </li>
                            <li class="nav-item px-lg-4">
                                <a class="nav-link text-uppercase text-expanded"
                                    href="http://127.0.0.1:8000/sobre">Sobre</a>
                            </li>
                            <li class="nav-item px-lg-4">
                                <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                                    href="http://127.0.0.1:8000/register">Cadastre-se</a>
                                {{-- <button onclick="window.location='{{url('register')}}'" class="btn btn-info mr-2 my-2
                                my-sm-0" type="button">Cadastrar</button> --}}
                            </li>
                            <li class="nav-item px-lg-4">
                                <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                                    href="http://127.0.0.1:8000/login">Login</a>
                                {{-- <button onclick="window.location='{{url('login')}}'" class="btn btn-success my-2
                                my-sm-0" type="button">Login</button> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </main>

    <section class="page-section clearfix">
        <div class="container">
            <div class="intro">
                <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="img/intro.jpg" alt="">
                <div class="intro-text left-0 text-center bg-faded p-5 rounded">
                    <h2 class="section-heading mb-4">
                        <span class="section-heading-upper">Mais café</span>
                        <span class="section-heading-lower">Para seu espaço de trabalho</span>
                    </h2>
                    <p class="mb-3">A maneira mais eficaz e pratica para sua "pausinha" pro café!
                    </p>
                    <div class="intro-button mx-auto">
                        <a class="btn btn-primary btn-xl" onclick="window.location='{{url('register')}}'"
                            href="#">Cadastre-se Já</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section clearfix">
        <div class="container text-center">
            <h4 class="site-heading text-center text-white d-none d-lg-block">Veja nossos cafés disponíveis!</h4>
            <!-- Example row of columns -->
            <div class="card-deck mx-auto">
                @foreach ($coffees as $coffee)
                <div class="row" style="color: #3a2a16;">
                    <div class="card mx-5 my-5" style="border-width: 6px; border-color: #592a01;
                background-color: #E7A046">
                        <h2 class="card-header" style="font-weight: 700">{{$coffee->name}}</h2>
                        <div class="card-body">
                            {{-- <h2 class="card-title">{{$coffee->name}}</h2> --}}
                            <p class="card-text" style="font-weight: 600">Quantidade disponível: {{$coffee->quantity}}</p>
                            <p class="card-text" style="font-weight: 600">Preço: R$ {{number_format($coffee->unit_price, '2', ',', '.')}}</p>
                            <p class="card-text" style="font-weight: 600">Dono: {{$coffee->owner_name}}</p>
                            @if ($coffee->quantity == 0)
                            <p style="font-weight: 700">ESGOTADO</p>
                            @else
                            <button type="button" class="btn btn-primary rounded" style="border-color: #774E19;
                         background-color: #3a2a16; font-weight: 700" onclick="window.location='{{url('coffee/'.$coffee->id)}}'">Ver
                                mais</button>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <hr>
        </div>
    </section>

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Nossa missão</span>
                            <span class="section-heading-lower">para você</span>
                        </h2>
                        <p class="mb-0">Se seu ambiente de trabalho curtiu nosso sistema não deixe
                            de compartilhar sua experiência com seus amigos e colegas, além disso ajudar os
                            desenvolvedores
                            irá melhorar o Coffe Break ainda mais para você.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@include ('.components.footer')

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>
