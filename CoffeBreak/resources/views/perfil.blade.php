<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Coffee-Break</title>
    <script src="js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
</head>


<body class="text-center" style="font-family: Raleway">

    @include ('components.nav')

    <div class="container" style="margin-top: 100px">
        <div class="card " style="border-width: 6px; border-color: #592a01; background-color: #E7A046;">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="font-weight:bold; color: #3a2a16">Seu perfil!</h4>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <div class="form-group row">
                                <label for="username" class="col-4 col-form-label" style="font-weight: 700; color: #3a2a16">Nome</label>
                                <div class="col-8">
                                    <input id="username" style=" border-color: #584021; border-width: 3px" name="username" placeholder="{{$name}}"
                                        class="form-control here" readonly type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-4 col-form-label" style="font-weight: 700; color: #3a2a16">Email</label>
                                <div class="col-8">
                                    <input id="email" name="email" style=" border-color: #584021; border-width: 3px" placeholder="{{$email}}" class="form-control here"
                                        readonly type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-4 col-form-label" style="font-weight: 700; color: #3a2a16">CPF</label>
                                <div class="col-8">
                                    <input id="website" style=" border-color: #584021; border-width: 3px" name="website" placeholder="{{$CPF}}" readonly
                                        class="form-control here" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-4 col-form-label" style="font-weight: 700; color: #3a2a16">Telefone</label>
                                <div class="col-8">
                                    <input id="website" style="border-color: #584021; border-width: 3px" name="website" placeholder="{{$telefone}}" readonly
                                        class="form-control here" type="text">
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                            <label for="newpass" class="col-4 col-form-label">New Password</label>
                            <div class="col-8">
                              <input id="newpass" name="newpass" placeholder="New Password" class="form-control here" type="text">
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="offset-4 col-8">
                              <button name="submit" type="submit" class="btn btn-primary">Update My Profile</button>
                            </div>
                          </div> --}}
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="my-3">
        <button type="button" style="border-color: #592a01;  color: #3a2a16; font-weight:600; border-width: 4px" class="btn btn-primary mt-5" onclick="window.location='{{url('meusCafes')}}'">Seus
            cafés</button>
            <button type="button" style="border-color: #592a01;  color: #3a2a16; font-weight:600; border-width: 4px" class="btn btn-primary mt-5" onclick="window.location='{{url('confirmarPagamentos')}}'">Confirmar pagamentos</button>
        <button type="button" style="border-color: #592a01;  color: #3a2a16; font-weight:600; border-width: 4px" class="btn btn-primary mt-5" onclick="window.location='{{url('pagarCafes')}}'">Pagar
            cafés</button>
        <button type="button" style="border-color: #592a01;  color: #3a2a16; font-weight:600; border-width: 4px" class="btn btn-primary mt-5" onclick="window.location='{{url('relatorioPegos')}}'">Histórico de cafés</button>
    </div>




</body>
</html>
