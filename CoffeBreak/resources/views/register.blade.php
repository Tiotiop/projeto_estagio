<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Coffe-Break Cadastro</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-center" style="font-family: Raleway">
    <main role="main">
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none"
                    href="http://127.0.0.1:8000/">Coffe Break</a>
                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button> --}}
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item active px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href="http://127.0.0.1:8000">Home
                            </a>
                        </li>
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                                href="http://127.0.0.1:8000/register">Cadastre-se</a>
                            {{-- <button onclick="window.location='{{url('register')}}'" class="btn btn-info mr-2 my-2
                            my-sm-0"
                            type="button">Cadastrar</button> --}}
                        </li>
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                                href="http://127.0.0.1:8000/login">Login</a>
                            {{-- <button onclick="window.location='{{url('login')}}'" class="btn btn-success my-2
                            my-sm-0"
                            type="button">Login</button> --}}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </main>
    <div class="">
        <h1 class="site-heading text-center text-white d-none d-lg-block">
            <span class="site-heading-upper text-primary mb-3">Bem vindos ao Coffe-Break</span>
            <span class="site-heading-lower">Logo, o café estará em sua mesa!
            </span>
        </h1>
        <h3 class="site-heading-lower text-center text-white d-none d-lg-block" style="font-family: Raleway">
            Preencha os
            espaços abaixo para o seu cadastro de usuário</h3>
    </div>
    <form method="POST" action="/register" class="form-signin my-3">
        @csrf
        <section class="page-section cta">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <label class="sr-only">Nome</label>
                        <input name="name" style="width: 520px; border-color: #584021; border-width: 4px"
                            class="form-control d-block my-3 mx-auto" placeholder="Nome" required="">

                        <label for="Endereço de email..." class="sr-only">Endereço de email...</label>
                        <input name="email" style="width: 520px; border-color: #584021; border-width: 4px" type="email"
                            id="inputEmail" class="form-control d-block my-3 mx-auto" style="width: 520px; "
                            placeholder="Endereço de email..." required="">

                        <label class="sr-only">CPF</label>
                        <input name="CPF" style="width: 520px; border-color: #584021; border-width: 4px" type="number"
                            style="width: 520px;" class="form-control d-block my-3 mx-auto" placeholder="CPF"
                            required="">

                        <label class="sr-only">Telefone</label>
                        <input name="telephone" style="width: 520px; border-color: #584021; border-width: 4px"
                            type="number" style="width: 520px;" class="form-control d-block my-3 mx-auto"
                            placeholder="Telefone" required="">

                        <label for="inputPassword" class="sr-only">senha</label>
                        <input name="password" style="width: 520px; border-color: #584021; border-width: 4px"
                            id="inputPassword" type="password" style="width: 520px;"
                            class="form-control d-block my-3 mx-auto" placeholder="Senha" required="">

                        <label for="password_confirm" class="sr-only">Confirme a senha</label>
                        <input name="password" style="width: 520px; border-color: #584021; border-width: 4px"
                            id="password_confirm" type="password" style="width: 520px;"
                            class="form-control d-block my-3 mx-auto" placeholder="Confirmar Senha" required="">
                        <button class="btn btn-lg btn-primary btn-block d-block mx-auto mt-4"
                            style="width: 180px; border-color: #584021; border-width: 4px"
                            type="submit">CADASTRAR</button>

                    </div>
                </div>
            </div>

    </form>
    </section>

</body>
@include('components.footer')

</html>
