<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Coffe-Break cafés cadastrados</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">


    <style>

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-center" style="font-family: Raleway">
    @include('components.nav')

    <br>
    <br>

    <div class="container mx-auto mt-5">
        <table class="table table-bordered table-hover text-center" style="background-color: #E7A046;
        border-width: 6px; border-color: #592a01; color: #3a2a16;">
            <tr>
                <th scope="col">Café</th>
                <th scope="col">Quantidade</th>
                <th scope="col">Preço</th>
                <th scope="col">Data de inserção</th>
                <th scope="col">Remoção</th>
            </tr>
            @foreach ($myCoffees as $coffee)
            <tr>
                <td style="font-weight: 500">{{$coffee->name}}</td>
                <td style="font-weight: 500">{{$coffee->quantity}}</td>
                <td style="font-weight: 500">{{number_format($coffee->unit_price, '2', ',', '.')}}</td>
                <td style="font-weight: 500">{{date("d/m/Y H:i", strtotime($coffee->created_at))}}</td>
                <td style="font-weight: 500">
                    <form action="/meusCafes" method="POST">
                        @csrf
                        <input name="id" type="hidden" value="{{$coffee->id}}">
                        <button class="btn rounded text-white" style="border-color: #774E19;
                        background-color: #4A300F" type="submit">Remover</button>
                    </form>
                </td>
            </tr>

            @endforeach
        </table>
    </div>

    <button class="btn btn-primary mt-3" type="button" style=" border-width: 3px; border-color: #592a01; color: #3a2a16; font-weight: 500"
        onclick="window.location='{{url('/perfil')}}'">Voltar</button>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
