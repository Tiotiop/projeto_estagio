<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Coffe-Break Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-center" style="font-family: Raleway">
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
        <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none"
                href="http://127.0.0.1:8000/">Coffe Break</a>
            {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button> --}}
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="http://127.0.0.1:8000">Home
                        </a>
                    </li>
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                            href="http://127.0.0.1:8000/register">Cadastre-se</a>
                        {{-- <button onclick="window.location='{{url('register')}}'" class="btn btn-info mr-2 my-2
                        my-sm-0"
                        type="button">Cadastrar</button> --}}
                    </li>
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" style="color: #e6a756"
                            href="http://127.0.0.1:8000/login">Login</a>
                        {{-- <button onclick="window.location='{{url('login')}}'" class="btn btn-success my-2
                        my-sm-0"
                        type="button">Login</button> --}}
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <form method="POST" action="/login" class="form-signin my-3">
        @csrf
        <h1 class="site-heading text-center text-white d-none d-lg-block">
            <span class="site-heading-upper text-primary mb-3">Bem vindo ao Coffe-Break</span>
            <span class="site-heading-lower">Por favor, faça seu login
            </span>
        </h1>
        <section class="page-section cta">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <label for="inputEmail" class="sr-only">Endereço de email...</label>
            <input name="email" type="email" id="inputEmail" class="form-control d-block my-3 mx-auto mb-2"
                style="width: 520px; border-color: #584021; border-width: 4px" placeholder="Email address" required="" autofocus="">
            <label for="inputPassword" class="sr-only">senha</label>
            <input name="password" type="password" id="inputPassword" style="width: 520px; border-color: #584021; border-width: 4px"
                class="form-control d-block my-3 mx-auto mb-2" placeholder="Password" required="">
            <!-- <div class="checkbox mb-3">
                <label>
                <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div> -->
            <button class="btn btn-lg btn-primary btn-block d-block mx-auto mt-3" style="width: 180px;
            border-color: #584021;
            border-width: 4px" type="submit">Entrar</button>
        </section>
    </form>

    @include('components.footer')
</body>

</html>
