<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Coffee-Break</title>
    <script src="js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="public/img/icon.ico" sizes="32x32" />
    <!-- Custom fonts for this template -->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">


    <style>
        body {
            color: #3a2a16;
        }
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
</head>


<body style="font-family: Raleway">

    @include ('components.nav')

    <main role="main">
        <div>
            <div class="container h-auto">
                <h3 class="site-heading text-center text-white d-none d-lg-block">
                    <span class="site-heading-lower" style="font-weight: 300;">Bem vindo ao Coffee-Break!</span>
                </h3>
                <h4 class="text-center text-white" style="font-weight: 300">Aqui você encontra os produtos cadastrados
                    pelos seus colegas!</h4></span>
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="card-deck">
                @foreach ($coffees as $coffee)
                <div class="row">
                    <div class="card mx-5 my-5" style="border-width: 6px; border-color: #592a01;
                background-color: #E7A046">
                        <h2 class="card-header" style="font-weight: 700">{{$coffee->name}}</h2>
                        <div class="card-body">
                            {{-- <h2 class="card-title">{{$coffee->name}}</h2> --}}
                            <p class="card-text" style="font-weight: 600">Quantidade disponível: {{$coffee->quantity}}</p>
                            <p class="card-text" style="font-weight: 600">Preço: R$ {{number_format($coffee->unit_price, '2', ',', '.')}}</p>
                            <p class="card-text" style="font-weight: 600">Dono: {{$coffee->owner_name}}</p>
                            @if ($coffee->quantity == 0)
                            <p style="font-weight: 700">ESGOTADO</p>
                            @else
                            <button type="button" class="btn btn-primary rounded" style="border-color: #774E19;
                         background-color: #3a2a16; font-weight: 700" onclick="window.location='{{url('coffee/'.$coffee->id)}}'">Ver
                                mais</button>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <hr>
        </div> <!-- /container -->
    </main>

    @include ('components.footer')

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script>
        window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')
    </script>
    <script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous">
    </script>

</body>

</html>
