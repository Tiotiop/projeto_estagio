<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Coffe-Break Cadastro de Item</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/sign-in/">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <link href="css/business-casual.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-center" style="font-family: Raleway">
    @include('components.nav')

    <div class="container" style="margin-top: 100px">
        <div class="card " style="border-width: 6px; border-color: #774E19; background-color: rgb(238, 156, 50)">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="color: #3a2a16">Insira as informações da capsula aqui!</h4>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-signin my-3" method="POST" action="/cadastroItem">
                            @csrf
                            <div class="form-group row">
                                <label class="sr-only">Nome do produto</label>
                                <input class="form-control d-block mx-auto" name="name"
                                    style="width: 520px; border-color: #584021; border-width: 4px"
                                    placeholder="Nome do produto" required="">
                            </div>
                            <div class="form-group row">
                                <label for="price" class="sr-only">Preço unitário</label>
                                <input class="form-control d-block mx-auto" name="unit_price" type="text" id="price"
                                    style="width: 520px; border-color: #584021; border-width: 4px " placeholder="Preço"
                                    required="">
                            </div>
                            <div class="form-group row">
                                <label class="sr-only">Quantidade</label>
                                <input class="form-control d-block mx-auto" name="quantity" min="0" type="number"
                                    style="width: 520px; border-color: #584021; border-width: 4px"
                                    placeholder="Quantidade" required="">
                            </div>
                            <div class="form-group row">
                                <label class="sr-only">Descricao</label>
                                <textarea class="form-control d-block mx-auto" name="description"
                                    style="width: 520px; border-color: #584021; border-width: 4px"
                                    placeholder="Descrição"></textarea>
                            </div>
                            <input type="hidden" name="owner_id" value="{{auth()->user()->id}}">
                            <input type="hidden" name="owner_name" value="{{auth()->user()->name}}">
                            <button class="btn btn-lg btn-primary btn-block d-block mx-auto mt-4"
                                style="width: 180px; border-color: #584021; border-width: 4px; background-color: #ffd48f; font-weight:500; color: #3a2a16"
                                type="submit">Cadastrar
                            </button>

                            <button onclick="window.location='{{url('/homeLoged')}}'"
                                class="btn btn-lg btn-primary btn-block d-block mx-auto mt-4"
                                style="width: 180px; border-color: #584021; background-color: rgb(53, 30, 11); border-width: 4px"
                                type="submit">Cancelar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <form class="form-signin my-3" method="POST" action="/cadastroItem">
        @csrf
        <img class="mb-4 mt-5"
            src="https://3.imimg.com/data3/RL/OA/MY-2455005/cosi-nespresso-coffee-capsules-250x250.png" alt=""
            width="86" height="86">

        <label class="sr-only">Nome do produto</label>
        <input class="form-control d-block my-3 mx-auto" name="name" style="width: 520px;" placeholder="Nome do produto"
            required="">

        <label for="price" class="sr-only">Preço unitário</label>
        <input class="form-control d-block my-3 mx-auto" name="unit_price" type="text" id="price" style="width: 520px; "
            placeholder="Preço" required="">

        <label class="sr-only">Quantidade</label>
        <input class="form-control d-block my-3 mx-auto" name="quantity" type="number" style="width: 520px;"
            placeholder="Quantidade" required="">

        <label class="sr-only">Descricao</label>
        <textarea class="form-control d-block my-3 mx-auto" name="description" style="width: 520px;"
            placeholder="Descrição"></textarea>

        <input type="hidden" name="owner_id" value="{{auth()->user()->id}}">
    <input type="hidden" name="owner_name" value="{{auth()->user()->name}}">
    <button class="btn btn-lg btn-primary btn-block d-block mx-auto mt-4"
        style="width: 180px;overflow:auto;resize:none;" type="submit">Cadastrar</button>
    <button onclick="window.location='{{url('/homeLoged')}}'"
        class="btn btn-lg btn-danger btn-block d-block mx-auto mt-4" style="width: 180px; "
        type="button">Cancelar</button>
    </form> --}}
</body>
</html>
