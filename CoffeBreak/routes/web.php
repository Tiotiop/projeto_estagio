<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/sobre', function () {
    return view('sobre');
});


Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/register', 'RegistrationController@create');
Route::post('/register','RegistrationController@store');

Route::get('/homeLoged', 'SessionsController@index');

Route::get('/perfil', 'ProfilesController@create');
Route::get('/meusCafes', 'ProfilesController@getCoffees');
Route::post('/meusCafes', 'ProfilesController@remove');

Route::get('/pagarCafes', 'ProfilesController@getUnpaidCoffeesByUser');
Route::post('/pagarCafes', 'OrdersController@pay');

Route::get('/relatorioPegos', 'ProfilesController@getTakenCoffees');

Route::get('/confirmarPagamentos', 'ProfilesController@getConfirmationRequests');
Route::post('/confirmarPagamentos', 'OrdersController@confirmPayment');

Route::get('/coffee/{id}', 'CoffeesController@coffeeInfo');
Route::post('/coffee/{id}', 'OrdersController@placeOrder');

Route::get('/cadastroItem', 'CoffeesController@create');
Route::post('/cadastroItem', 'CoffeesController@store');

Route::get('/teste/{id}', 'ProfilesController@getOwed');
