<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->foreignId('user_id');
            $table->string('order_user');
            $table->foreignId('coffee_id');
            $table->string('variety');
            $table->foreignId('owner_id');
            $table->string('owner');
            $table->string('owner_email');
            $table->string('owner_phone');
            $table->integer('order_quantity');
            $table->double('total_price', 100,2);
            $table->boolean('user_payment_confirm');
            $table->boolean('owner_payment_confirm');
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('coffee_id')->references('id')->on('coffees');
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
