<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoffeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coffees', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->double('unit_price', 100, 2);
            $table->integer('quantity');
            $table->text('description')->nullable();
            $table->foreignId('owner_id');
            $table->string('owner_name');
            $table->timestamps();
        });

        Schema::table('coffees', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coffees');
    }
}
